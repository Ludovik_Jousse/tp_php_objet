<?php

use App\App;

function dd($e)
{
    var_dump($e);
    exit;
}

session_start();
const DS = DIRECTORY_SEPARATOR;
const PATH_ROOT = __DIR__ . DS;

require_once PATH_ROOT . 'vendor' . DS . 'autoload.php';

App::startApp()->start();
