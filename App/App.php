<?php

namespace App;

use ApertureCore\Database\DatabaseConfig;
use ApertureCore\View;
use App\controller\AjoutAnnonce;
use App\controller\connexion;
use App\controller\details;
use App\controller\FavoriController;
use App\controller\ListeAnnonce;
use App\controller\ListeReservations;
use App\controller\testController;
use MiladRahimi\PhpRouter\Router;


class App implements DatabaseConfig
{
    private static ?self $instance = null;
    private const DB_HOST = 'database';
    private const DB_PASS = 'lamp';
    private const DB_USER = 'lamp';
    private const DB_NAME = 'lamp';

    private Router $router ;

    public static function startApp(): self
    {
        if(is_null(self::$instance)) self::$instance = new self();

        return self::$instance;
    }


    private function __construct(){
        $this->router = Router::create();
    }

    #region Interface database functions
    public function getHost(): string
    {
        return self::DB_HOST;
    }

    public function getName(): string
    {
        return self::DB_NAME;
    }

    public function getUser(): string
    {
        return self::DB_USER;
    }

    public function getPass(): string
    {
        return self::DB_PASS;
    }

    #endregion

    public function start() : void
    {
        $this->registerNewRoutes();
        $this->startRouter();

    }

    private function registerNewRoutes() : void
    {
        $this->router->pattern('id',  '[1-9]\d*');

        $this->router->any('/',[connexion::class,'connexion']);
        $this->router->any('/?inscription=1',[connexion::class,'connexion']);
        $this->router->any('/inscription_reussie',[connexion::class,'reussie']);
        $this->router->any('/nouvelle-annonce',[AjoutAnnonce::class,'creationAnnonce']);
        $this->router->any('/liste',[ListeAnnonce::class,'listeAnnonce']);
        $this->router->any('/reservations',[ListeReservations::class,'listeReservations']);
        $this->router->get('/detail/{id}',[details::class,'detail']);
        $this->router->any('/favoris',[FavoriController::class,'listeFavori']);
        $this->router->any('/test',[testController::class,'test']);
        $this->router->any('/easter-egg',[testController::class,'easterEgg']);

    }

    private function startRouter() : void
    {
        try {
            $this->router->dispatch();

        }catch (\Exception $e){
            switch ($e->getCode()) {
                case 1:
                    $_SESSION['connexion_bug'] = 1;
                    header('Location: /');
                    break;
                case 2:
                    View::renderError(400);
                    break;
                case 4:
                    $_SESSION['new_annonce_bug'] = 1;
                    header('Location: /nouvelle-annonce');
                    break;
                case 5:
                    $_SESSION['booking_bug'] = 1;

                    header('Location: /detail/'. $_SESSION['rental_id']);
                    break;

                case 3 :
                default :
                    View::renderError(404);
                    break;
            }
        }
    }

    #region fonction du singleton
    private function __clone(){}
    private function __wakeup(){}
    #endregion
}