<?php

namespace App\controller;

use ApertureCore\Http\Exceptions\ServeurException;
use ApertureCore\pageController;
use ApertureCore\View;
use App\AppRepositoryManager;

class ListeReservations extends pageController
{

    /**
     * Fonction pour afficher la liste des réservations
     *
     * @throws ServeurException
     */
    public function listeReservations()
    {
        $this->middlewareSession();
        $liste = $this->ListReservationsByUser();

        $view = new View('pages/listeReservations');

        $view_data = [
            'nav_menu' => $this->navMenuByUser(),
            'list' => $liste,
            'title_tag' => 'Reservations'
        ];
        $view->render($view_data);
    }


    /**
     * Fonction pour déterminer quelle liste on doit afficher
     *
     * @return array
     * @throws ServeurException
     */
    public function ListReservationsByUser(): array
    {

        if($_SESSION['user_type'] == 1){
            $liste = AppRepositoryManager::getRm()->getBookingsRepository()->listBookingsOfOwner($_SESSION['user_id']);
        }else{
            $liste = AppRepositoryManager::getRm()->getBookingsRepository()->listBookingsOfUser($_SESSION['user_id']);
        }

        return $liste;
    }
}