<?php

namespace App\controller;

use ApertureCore\pageController;
use ApertureCore\View;
use App\AppRepositoryManager;

class AjoutAnnonce extends pageController
{

    public function creationAnnonce()
    {
        if(!empty($_SESSION['annonce_flag'])) {
            $_SESSION['new_annonce_bug'] = 0;
            $_SESSION['annonce_flag'] = 0;
        }

        if(!empty($_SESSION['new_annonce_bug'])){
            $_SESSION['annonce_flag'] = 1;
        }

        $view_data = [
            'title_tag' => 'Ajout annonce',
            'nav_menu' => $this->navMenuByUser(),
            'date' => new \DateTime()
        ];
        $view = new View('pages/creation-annonces');
        $view->render($view_data);
    }
}