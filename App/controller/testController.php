<?php

namespace App\controller;

use ApertureCore\pageController;
use ApertureCore\View;
use App\AppRepositoryManager;

class testController extends pageController
{

    public function test()
    {
        $view = new View('pages/test');

        $view_data = [
            'list' => AppRepositoryManager::getRm()->getBookingsRepository()->listBookingsOfOwner(3)
        ];

        $view->render($view_data);


    }

    public function easterEgg()
    {

        $view = new View('pages/easter_egg', true);

        $view->render();

    }
}