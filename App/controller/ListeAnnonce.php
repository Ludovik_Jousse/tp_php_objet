<?php

namespace App\controller;

use ApertureCore\Http\Exceptions\AnnonceException;
use ApertureCore\Http\Exceptions\BookingException;
use ApertureCore\Http\Exceptions\ConnexionException;
use ApertureCore\Http\Exceptions\ServeurException;
use ApertureCore\pageController;
use ApertureCore\View;
use App\AppRepositoryManager;
use Exception;

class ListeAnnonce extends pageController
{


    /**Render pour la page de liste
     *
     * @return void
     * @throws ServeurException
     * @throws ConnexionException
     */
    public function listeAnnonce()
    {
        $this->middlewareSession();
        $this->annonceController();

        $view = new View('pages/list');

        if ($_SESSION['user_type'] == 1) {
            $arr = AppRepositoryManager::getRm()->getRentalsRepository()->listRentalsByOwner($_SESSION['user_id']);
        } else {
            $arr = AppRepositoryManager::getRm()->getRentalsRepository()->listAllRentals();
        }

        $view_data = [
            'list' => $arr,
            'nav_menu' => $this->navMenuByUser(),
            'title_tag' => 'Liste'
        ];

        $view->render($view_data);

    }

    /**
     * Fonction qui permet de contrôler toutes les entrées de formulaire
     *
     * @throws ConnexionException
     * @throws ServeurException
     * @throws AnnonceException
     * @throws BookingException
     */
    public function annonceController()
    {
        if (!empty($_POST)) {

            if (!empty($_POST['city'])) {

                $this->formAddRental($_POST);

            } elseif (!empty($_POST['resa'])) {

                $this->middleWareDate($_POST);

                AppRepositoryManager::getRm()->getBookingsRepository()->addBooking($_POST);

            } elseif (!empty($_POST['mail'])) {

                $this->formUserController($_POST);

            }

            // Permet de ne pas avoir le $_POST quand on refresh
            $this->returnToSelf('/liste');

        }
    }


    #region fonction de contrôle

    /**
     * Fonction qui ajoute un rental
     *
     * @param array $data
     *
     * @return void
     * @throws ServeurException
     * @throws AnnonceException
     * @throws BookingException
     */
    private function formAddRental(array $data)
    {

        $this->middleWareDate($data);

        AppRepositoryManager::getRm()->getAddressRepository()->addAddress($data);
        $address_id = AppRepositoryManager::getRm()->getAddressRepository()->getAddressIdByCity($data['city']);
        AppRepositoryManager::getRm()->getRentalsRepository()->AddOneRental($data, $address_id);
        $rental_id = AppRepositoryManager::getRm()->getRentalsRepository()->GetOneRentalIdByName($data['name']);

        if (!empty($data['equipement'])) {
            AppRepositoryManager::getRm()->getEquipmentRepository()->AddEquipementList($rental_id, $data['equipement']);

        } else {
            AppRepositoryManager::getRm()->getEquipmentRepository()->AddEquipementList($rental_id, []);
        }
    }


    /**
     * Fonction qui ajoute si besoin un utilisateur et qui le connecte avec le $_SESSION
     *
     * @param array $data
     *
     * @return void
     * @throws ServeurException
     * @throws ConnexionException
     */
    private function formUserController(array $data)
    {
        if (!empty($data['name'])) {
            AppRepositoryManager::getRm()->getUsersRepository()->addOneUser($data);
        }

        $user = AppRepositoryManager::getRm()->getUsersRepository()->getOneUser($data);

        $_SESSION['user_id'] = $user->id;
        $_SESSION['user_type'] = $user->type;

    }

    /**
     * @param array $data
     *
     * @return void
     * @throws AnnonceException
     * @throws BookingException
     * @throws Exception
     */
    private function middleWareDate(array $data)
    {
        $start_date = $data['checkin-year'] . '-' . $data['checkin-month'] . '-' . $data['checkin-day'];
        $end_date = $data['checkout-year'] . '-' . $data['checkout-month'] . '-' . $data['checkout-day'];

        $date = new \DateTime($start_date);
        $date2 = new \DateTime($end_date);
        $date3 = new \DateTime();

        if ($date->getTimestamp() < $date3->getTimestamp() || $date2->getTimestamp() < $date3->getTimestamp()) {
            if (!empty($_POST['resa'])) {
                throw new BookingException();
            }
            throw new AnnonceException();
        }
    }

    #endregion
}