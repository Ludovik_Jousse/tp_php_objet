<?php

namespace App\controller;

use ApertureCore\pageController;
use ApertureCore\View;
use App\AppRepositoryManager;

class details extends pageController
{


    public function detail(int $id)
    {
        if (!empty($_SESSION['booking_flag'])) {
            $_SESSION['booking_bug'] = 0;
            $_SESSION['booking_flag'] = 0;
        }

        if (!empty($_SESSION['booking_bug'])) {
            $_SESSION['booking_flag'] = 1;
        }

        $view = new  View('pages/detail');

        $_SESSION['rental_id'] = $id;

        $view_data = [
            'nav_menu' => $this->navMenuByUser(),
            'list' => AppRepositoryManager::getRm()->getRentalsRepository()->GetOneRentalById($id),
            'favorite' => AppRepositoryManager::getRm()->getFavoriteRepository()
                ->selectFavoriteRental($_SESSION['user_id'], $_SESSION['rental_id']),
            'title_tage' => 'Detail',
            'date' => new \DateTime()
        ];


        $view->render($view_data);
    }
}