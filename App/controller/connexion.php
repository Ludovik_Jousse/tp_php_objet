<?php

namespace App\controller;

use ApertureCore\pageController;
use ApertureCore\View;

class connexion extends pageController
{

    public function connexion()
    {

        if(!empty($_SESSION['connexion_flag'])) {
            $_SESSION['connexion_bug'] = 0;
            $_SESSION['connection_flag'] = 0;
        }

        if(!empty($_SESSION['connexion_bug'])){
            $_SESSION['connexion_flag'] = 1;
        }

        if(isset($_POST['session-end'])) session_unset();

        if (empty($_SESSION['connexion_bug']) && !empty($_SESSION['user_type'])) header('Location: /liste');

        $view = new View('pages/connexion');

        $view_data = [
            'nav_menu' => [],
            'title_tag' => 'Connexion'
        ];

        $view->render($view_data);
    }


}