<?php

namespace App\controller;

use ApertureCore\Http\Exceptions\ServeurException;
use ApertureCore\pageController;
use ApertureCore\View;
use App\AppRepositoryManager;

class FavoriController extends pageController
{

    /**Fonction qui permet d'afficher la liste des favoris
     *
     * @throws ServeurException
     */
    public function listeFavori()
    {
        $this->middlewareSession();
        $this->FormController();

        $view = new View('pages/favori');

        $view_data = [
            'nav_menu' => $this->navMenuByUser() ,
            'list' => AppRepositoryManager::getRm()->getFavoriteRepository()->getFavoriteList($_SESSION['user_id']),
            'title_tag' => 'Favoris'
        ];

        $view->render($view_data);
    }


    /**Fonction qui permet d'ajouter un nouveau favori
     *
     * @return void
     */
    public function FormController()
    {
        if(!empty($_POST)){
            AppRepositoryManager::getRm()->getFavoriteRepository()
                ->addFavorite($_SESSION['user_id'], $_SESSION['rental_id'] );

            // Permet de ne pas avoir le $_POST quand on refresh
            $this->returnToSelf('/favoris');
        }
    }
}