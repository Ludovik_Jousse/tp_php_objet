<?php

namespace App;

use ApertureCore\RepositoryManager;
use App\App;
use App\model\Repository\addressRepository;
use App\model\Repository\bookingsRepository;
use App\model\Repository\equipmentRepository;
use App\model\Repository\favoriteRepository;
use App\model\Repository\rentalsRepository;
use App\model\Repository\usersRepository;

class AppRepositoryManager
{

    use RepositoryManager;

    private bookingsRepository $bookingsRepository;
    private equipmentRepository  $equipmentRepository;
    private rentalsRepository $rentalsRepository;
    private usersRepository $usersRepository;
    private addressRepository $addressRepository;
    private favoriteRepository $favoriteRepository;


    public function __construct()
    {
        $config = App::startApp();

        $this->bookingsRepository = new bookingsRepository($config);
        $this->equipmentRepository = new equipmentRepository($config);
        $this->rentalsRepository = new rentalsRepository($config);
        $this->usersRepository = new usersRepository($config);
        $this->addressRepository = new addressRepository($config);
        $this->favoriteRepository = new favoriteRepository($config);
    }


    #region Get Functions

    /**
     * @return addressRepository
     */
    public function getAddressRepository(): addressRepository
    {
        return $this->addressRepository;
    }

    /**
     * @return bookingsRepository
     */
    public function getBookingsRepository(): bookingsRepository
    {
        return $this->bookingsRepository;
    }

    /**
     * @return equipmentRepository
     */
    public function getEquipmentRepository(): equipmentRepository
    {
        return $this->equipmentRepository;
    }

    /**
     * @return rentalsRepository
     */
    public function getRentalsRepository(): rentalsRepository
    {
        return $this->rentalsRepository;
    }

    /**
     * @return usersRepository
     */
    public function getUsersRepository(): usersRepository
    {
        return $this->usersRepository;
    }

    /**
     * @return favoriteRepository
     */
    public function getFavoriteRepository(): favoriteRepository
    {
        return $this->favoriteRepository;
    }

#endregion

}