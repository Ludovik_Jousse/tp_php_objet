<?php

namespace App\model;

use ApertureCore\Model;

class Adresse extends Model
{
    public int $id;
    public string $country;
    public string $city;
}