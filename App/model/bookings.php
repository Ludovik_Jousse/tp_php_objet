<?php

namespace App\model;

use ApertureCore\Model;


class bookings extends Model
{
    public int $id;
    public int $user_id;
    public int $rental_id;
    public string $check_in;
    public string $check_out;
    public array $rentals;
    public users $users;
}