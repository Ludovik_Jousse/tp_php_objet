<?php

namespace App\model\Repository;

use ApertureCore\Http\Exceptions\ServeurException;
use ApertureCore\Repository;
use App\AppRepositoryManager;
use App\model\bookings;
use App\model\users;
use DateInterval;
use DateTime;

class bookingsRepository extends Repository
{

    protected function getTableName(): string
    {
        return 'bookings';
    }

    /**Fonction pour aller chercher la liste des réservations d'un annonceur
     *
     * @param int $rental_id Id de l'annonceur
     *
     * @return array
     * @throws ServeurException
     * @throws \Exception
     */
    public function listBookingsOfOwner(int $owner_id) : array
    {
        $row_data = [];

        $q = "select bookings.id, user_id, rental_id, check_in, check_out from bookings 
    join rentals r on bookings.rental_id = r.id where owner_id = :owner_id";

        $stmt = $this->pdo->prepare($q);

        $stmt->execute(['owner_id' => $owner_id ]);

        if (!$stmt) throw new ServeurException();

        while ($row = $stmt->fetch()){

            $data = new bookings($row);

            $data->users = AppRepositoryManager::getRm()->getUsersRepository()->readById(users::class, $data->user_id);
            $data->rentals = AppRepositoryManager::getRm()->getRentalsRepository()->GetOneRentalById($data->rental_id);

            $row_data[] = $data;
        }
        return $row_data;

    }

    /**Fonction pour aller chercher la liste des annonces pour un seul utilisateur
     *
     * @param int $user_id
     *
     * @return array
     * @throws ServeurException
     * @throws \Exception
     */
    public function listBookingsOfUser(int $user_id) : array
    {
        $row_data = [];

        $q = 'select id, user_id, rental_id, check_in, check_out from bookings where user_id = :user_id';

        $stmt = $this->pdo->prepare($q);

        $stmt->execute(['user_id' => $user_id]);

        if (!$stmt) throw new ServeurException();

        while ($row = $stmt->fetch()) {
            $data = new bookings($row);

            $data->check_in = date('d-m-Y', strtotime($data->check_in) );
            $data->check_out = date('d-m-Y', strtotime($data->check_out));

            $data->users = AppRepositoryManager::getRm()->getUsersRepository()->readById(users::class, $data->user_id);
            $data->rentals = AppRepositoryManager::getRm()->getRentalsRepository()->GetOneRentalById($data->rental_id);

            $row_data[] = $data;
        }

        return $row_data;

    }


    /**
     * Fonction pour ajouter une réservation
     *
     * @param array $data
     *
     * @return void
     * @throws ServeurException
     * @throws \Exception
     */
    public function addBooking(array $data)
    {

        $q = 'insert into bookings(user_id, rental_id, check_in, check_out) VALUES (:user_id,:rental_id,:check_in,:check_out)';

        $check_in = $_POST['checkin-year'] . '-' . $_POST['checkin-month'] . '-' . $_POST['checkin-day'] ;
        $check_out = $_POST['checkout-year'] . '-' . $_POST['checkout-month'] . '-' . $_POST['checkout-day'] ;


        $stmt = $this->pdo->prepare($q);
        $stmt->execute([
            ':user_id' => $_SESSION['user_id'],
            ':rental_id' => $_SESSION['rental_id'],
            ':check_in' => $check_in,
            ':check_out' => $check_out
        ]);



        $last_id = $this->pdo->lastInsertId();

        $time = new DateTime($check_in);
        $time2 = new DateTime($check_out);

        $ii = 1;

        $flag = ($time->diff($time2)->days);

        $q = 'INSERT INTO booking_date(id_booking, check_in, check_out)
                VALUES (:id,:check_in,:check_out)';

        while ($ii <= $flag) {

            $stmt = $this->pdo->prepare($q);

            $check_in = $time->format('Y-m-d');
            $check_out = $time->add(new DateInterval('P1D'))->format('Y-m-d');

            $stmt->execute([
                ':id' => $last_id,
                ':check_in' => $check_in,
                ':check_out' => $check_out,
            ]);

            $ii++;
        }

        if (!$stmt) throw new ServeurException();

    }
}