<?php

namespace App\model\Repository;

use ApertureCore\Http\Exceptions\AnnonceException;
use ApertureCore\Http\Exceptions\ServeurException;
use App\model\Equipment;

class equipmentRepository extends \ApertureCore\Repository
{

    protected function getTableName(): string
    {
        return 'equipments';
    }

    /**Fonction pour avoir la liste des equipements pour une annonce
     *
     * @param int $id
     *
     * @return array
     * @throws ServeurException
     */
    public function getEquipmentList(int $id) : array
    {
        $data_row = [];

        $q = "select equipments.id as id, label from equipments join rental_equipment re on equipments.id = re.equipment_id
join rentals r on r.id = re.rental_id where rental_id = :id";

        $stmt = $this->pdo->prepare($q);

        $stmt->execute([':id' => $id]);

        if (!$stmt) throw new ServeurException();

        while($row = $stmt->fetch()){
            $data_row[] = new Equipment($row);
        }

        return $data_row;
    }


    /**Fonction pour aller ajouter une liste d'équipement pour une annonce dans la bdd
     *
     * @param int   $rental_id
     * @param array $equipements
     *
     * @return void
     * @throws ServeurException
     */
    public function AddEquipementList(int $rental_id, array $equipements) : void
    {

        foreach ($equipements as $item){
        $q = 'insert into rental_equipment(rental_id, equipment_id) VALUES (:rental_id, :equipement_id)';

        $stmt = $this->pdo->prepare($q);

        $stmt->execute([':rental_id' => $rental_id, ':equipement_id' => $item]);

            if (!$stmt) throw new AnnonceException();

        }

    }

}