<?php

namespace App\model\Repository;

use ApertureCore\Http\Exceptions\AnnonceException;
use ApertureCore\Http\Exceptions\ServeurException;
use ApertureCore\Repository;
use App\model\Adresse;

class addressRepository extends Repository
{

    protected function getTableName(): string
    {
        return 'addresses';
    }

    /**Fonction qui permet d'ajouter une adresse dans la base de données
     *
     * @param array $data Tableau associatif, toutes les données pour chaque colonne
     *
     * @return void
     * @throws ServeurException
     */
    public function addAddress(array $data): void
    {

        $city = $data['city'];
        $country = $data['country'];

        $q = 'insert into addresses(city, country) values (:city,:country);';

        $stmt = $this->pdo->prepare($q);

        $stmt->execute([':city' => $city, ':country' => $country]);

        if (!$stmt) throw new AnnonceException();
    }


    /**
     * Fonction qui permet d'aller chercher l'id d'une adresse, utilisée pour rentrer une nouvelle annonce
     *
     * @param string $id
     *
     * @return Adresse|null
     * @throws ServeurException
     *
     */
    public function getOneAddressId(string $id): ?Adresse
    {
        $q = "select id, city, country from addresses where id = :id";

        $stmt = $this->pdo->prepare($q);

        $stmt->execute([':id' => $id ]);

        if (!$stmt) throw new ServeurException();

        $row_data = $stmt->fetch();


        return new Adresse($row_data) ;
    }


    /**
     * Fonction qui permet d'aller chercher une adresse en fonction de la ville
     *
     * @param string $city
     *
     * @return int
     * @throws ServeurException
     */
    public function getAddressIdByCity(string $city) : int
    {
        $q = 'SELECT id from addresses where city = :city';

        $stmt = $this->pdo->prepare($q);

        $stmt->execute([':city' => $city]);

        if (!$stmt) throw new ServeurException();

        $row = $stmt->fetch();

        return intval($row['id']);

    }

}
