<?php

namespace App\model\Repository;

use ApertureCore\Http\Exceptions\ConnexionException;
use ApertureCore\Http\Exceptions\RouteNotFoundException;
use ApertureCore\Http\Exceptions\ServeurException;
use ApertureCore\Repository;
use App\model\users;

class usersRepository extends Repository
{

    protected function getTableName(): string
    {
        return 'users';
    }

    public function addOneUser(array $data):void
    {
        $q = 'insert into users(email, pass, type, name) VALUES (:mail,:pass,:type,:name)';

        $stmt = $this->pdo->prepare($q);

        $stmt->execute([
            ':mail' => $data['mail'],
            ':pass' => $data['pass'],
            ':type' => $data['user-type'],
            ':name' => $data['name']
        ]);

        if (!$stmt) throw new ServeurException();
    }

    public function getOneUser(array $data) : ?users
    {
        $q = 'select id,name,type from users where email = :email and pass=:pass';

        $stmt = $this->pdo->prepare($q);

        $stmt->execute([
            ':email' => $data['mail'],
            ':pass' => $data['pass']
        ]);

        $row = $stmt->fetch();

        if (!$row) throw new ConnexionException();
        $_SESSION['connexion_bug'] = 0;
        return new users($row);
    }
}