<?php

namespace App\model\Repository;

use ApertureCore\Http\Exceptions\AnnonceException;
use ApertureCore\Http\Exceptions\ServeurException;

use ApertureCore\Repository;
use Cassandra\Date;


class rentalsRepository extends Repository
{

    protected function getTableName(): string
    {
        return 'rentals';
    }


    /**Fonction pour aller chercher toutes les annonces avec la jointure de la table addresses
     *
     * @return array
     * @throws ServeurException
     */
    public function listAllRentals(): array
    {

        $q = "select rentals.id ,owner_id,type,surface,description,capacity,price,address_id, name, start_date, end_date,image
                from rentals";

        $stmt = $this->pdo->query($q);

        if (!$stmt) throw new ServeurException();

        return $this->autoRentalReturn($stmt);

    }


    /**Fonction pour aller chercher la liste des annonces pour un seul annonceur
     *
     * @param int $owner_id id de l'annonceur
     *
     * @return array
     * @throws ServeurException
     */
    public function listRentalsByOwner(int $owner_id): array
    {
        $row_data = [];

        $q = "select id ,owner_id,type,surface,description,capacity,price,address_id, name, start_date, end_date,image
                from rentals
                where owner_id = :id;";

        $stmt = $this->pdo->prepare($q);

        $stmt->execute([':id' => $owner_id]);

        if (!$stmt) throw new ServeurException();

        return $this->autoRentalReturn($stmt);

    }


    /**Fonction pour aller chercher une annonce en fonction de son nom
     *
     * @param string $name
     *
     * @return int
     * @throws ServeurException
     */
    public function GetOneRentalIdByName(string $name): int
    {
        $q = 'select id from rentals where name = :name';

        $stmt = $this->pdo->prepare($q);

        $stmt->execute([':name' => $name] );


        $row = $stmt->fetch();

        if (!$stmt) throw new ServeurException();


        return intval($row['id']);
    }

    /** Fonction pour aller chercher une seule annonce
     *
     * @param int $rental_id
     *
     * @return array
     * @throws ServeurException
     */
    public function GetOneRentalById(int $rental_id): array
    {
        $row_data = [];

        $q = "select id ,owner_id,type,surface,description,capacity,price,address_id, name, start_date, end_date,image
                from rentals
                where id = :rid";

        $stmt = $this->pdo->prepare($q);
        $stmt->execute([':rid' => $rental_id]);

        if (!$stmt) throw new ServeurException();

        return $this->autoRentalReturn($stmt);

    }


    /**Fonction pour ajouter une annonce
     *
     * @param array $data Tableau associatif avec toutes les données pour chaque colonne
     * @param int   $address_id
     *
     * @return void
     * @throws ServeurException
     */
    public function AddOneRental(array $data, int $address_id): void
    {

        $q = "insert into rentals(
                    owner_id,
                    type,
                    surface,
                    description,
                    capacity,
                    price,
                    address_id,
                    name, 
                    start_date,
                    end_date,
                    image)
                VALUES (
                    :owner_id,
                    :type,
                    :surface,
                    :description,
                    :capacity,
                    :price,
                    :address_id,
                    :name,
                    :start_date,
                    :end_date,
                     :image   
                        
 );";

        $stmt = $this->pdo->prepare($q);

        $uploadimg = PATH_ROOT . 'assets'. DS .'img'. DS ;
        $uploadfile = $uploadimg . basename($_FILES['file']['name']);

        move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile );


        $start_date = $_POST['checkin-year'] . '-' . $_POST['checkin-month'] . '-' . $_POST['checkin-day'];
        $end_date = $_POST['checkout-year'] . '-' . $_POST['checkout-month'] . '-' . $_POST['checkout-day'];

        $stmt->execute([
            ':owner_id' => $_SESSION['user_id'],
            ':type' => $data['type'],
            ':surface' => $data['surface'],
            ':description' => $data['description'],
            ':capacity' => $data['capacity'],
            ':price' => $data['price'],
            ':address_id' => $address_id,
            ':name' => $data['name'],
            ':start_date' => $start_date,
            ':end_date' => $end_date,
            'image' => $_FILES['file']['name']
        ]);

        if (!$stmt) throw new AnnonceException();

    }

}