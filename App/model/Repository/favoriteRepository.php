<?php

namespace App\model\Repository;

use ApertureCore\Http\Exceptions\ServeurException;
use App\AppRepositoryManager;
use App\model\favori;

class favoriteRepository extends \ApertureCore\Repository
{

    protected function getTableName(): string
    {
        return 'favorite';
    }

    /**
     * @throws ServeurException
     */
    public function getFavoriteList(int $id_user): array
    {
        $data = [];

        $q = 'SELECT * from favorite where id_user = :id_user';

        $stmt = $this->pdo->prepare($q);

        $stmt->execute([':id_user' => $id_user]);

        while ($row = $stmt->fetch()) {

            $row_data = new favori($row);
            $row_data->rentals = AppRepositoryManager::getRm()->getRentalsRepository()
                ->GetOneRentalById($row_data->id_rental);

            $data[] = $row_data;

        }

        return $data;
    }


    /**
     * Fonction pour ajouter un favori pour un certain utilisateur
     *
     * @param int $id_user
     * @param int $id_rental
     *
     * @return void
     */
    public function addFavorite(int $id_user, int $id_rental): void
    {

        $flag = $this->selectFavoriteRental($id_user, $id_rental);

        if(!$flag){
            $q = 'insert into favorite(id_user, id_rental) VALUES (:id_user, :id_rental)';

        }else{
            $q = 'delete from favorite where id_rental = :id_rental and id_user = :id_user';
        }

        $stmt = $this->pdo->prepare($q);

        $stmt->execute([
            ':id_user' => $id_user,
            ':id_rental' => $id_rental
        ]);

    }


    /**
     * Fonction pour savoir si le rental fait partie de la liste de favori de l'utilisateur
     *
     * @param int $id_rental
     * @param int $id_user
     *
     * @return array
     */
    public function selectFavoriteRental(int $id_user, int $id_rental): array
    {

        $data = [];

        $query = 'SELECT * from favorite where id_rental = :id_rental AND id_user = :id_user';

        $sth = $this->pdo->prepare($query);

        $sth->execute([
           ':id_rental' => $id_rental,
            ':id_user' => $id_user
        ]);

        while($row = $sth->fetch()){
            $data[] = $row;
        }

        return $data;
    }

}