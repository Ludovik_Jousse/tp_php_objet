<?php

namespace App\model;

use ApertureCore\Model;

class Equipment extends Model
{
    public int $id;
    public string $label;
}