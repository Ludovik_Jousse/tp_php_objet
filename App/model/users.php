<?php

namespace App\model;

use ApertureCore\Model;

class users extends Model
{
    public int $id;
    public string $name;
    public string $email;
    public string $pass;
    public int $type;
}