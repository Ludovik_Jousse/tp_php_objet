<?php

namespace App\model;

use ApertureCore\Model;

class rentals extends Model
{
    public int $id;
    public int $owner_id;
    public int $type;
    public string $name;
    public float $surface;
    public string $description;
    public int $capacity;
    public float $price;
    public int $address_id;
    public string $start_date;
    public string $end_date;
    public array $equipment;
    public Adresse $address;
    public string $image;

}