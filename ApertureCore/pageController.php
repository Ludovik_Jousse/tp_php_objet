<?php

namespace ApertureCore;

class pageController
{
    /**Fonction qui permet de changer le menu dans la nav en fonction du type d'utilisateur
     *
     * @return array
     */
    protected function navMenuByUser() : array
    {

        if ($_SESSION['user_type'] === 1) {
            $arr = [
                'Voir vos annonces' => 'liste',
                'Ajouter une annonce' => 'nouvelle-annonce',
                'Voir vos réservations' => 'reservations'
            ];
        } else {
            $arr = [
                'Liste des annonces' => 'liste',
                'Vos réservations' => 'reservations',
                'Vos favoris' => 'favoris'
            ];
        }

        return $arr;
    }

    protected function middlewareSession()
    {
        if (empty($_SESSION['user_type'])) header('Location: /');
    }

    protected function returnToSelf(string $route)
    {
        header('Location: '.$route);
    }

}