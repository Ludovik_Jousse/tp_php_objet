<?php

namespace ApertureCore\Http\Exceptions;

class BookingException extends \Exception implements \Throwable
{
    public function __construct( Throwable $previous = null)
    {
        parent::__construct('Problème dans la réservation', 5, $previous);
    }
}