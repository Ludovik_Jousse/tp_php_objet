<?php

namespace ApertureCore\Http\Exceptions;


use Throwable;

class ConnexionException extends \Exception implements \Throwable
{


    public function __construct( Throwable $previous = null)
    {
        parent::__construct('Erreur de connexion', 1, $previous);
    }

}