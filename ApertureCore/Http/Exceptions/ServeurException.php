<?php

namespace ApertureCore\Http\Exceptions;


use Throwable;

class ServeurException extends \Exception implements \Throwable
{

    public function __construct(Throwable $previous = null)
    {
        parent::__construct("Problème de serveur, veuillez nous excuser", 2, $previous);
    }
}