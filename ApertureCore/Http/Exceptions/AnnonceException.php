<?php

namespace ApertureCore\Http\Exceptions;

class AnnonceException extends \Exception implements \Throwable
{
    public function __construct( Throwable $previous = null)
    {
        parent::__construct('Problème avec les données reçus, veuillez réessayer', 4, $previous);
    }
}