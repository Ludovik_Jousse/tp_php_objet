<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css'
          integrity='sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T' crossorigin='anonymous'>
    <link rel="stylesheet" href="../../assets/css/style.css">
    <title><?= $title_tag ?> - AirPnP</title>
</head>
<body>

<header>
    <section id="header-section" class="pb-2">
        <div class='container'>
            <a href="/" class="logo-container">
                <img id="logo" src="../../assets/img/airbnb.png">
                <span>Airpnp</span>
            </a>
            <nav class="d-flex flex-row justify-content-between p-2 row">
                <?php if (!empty($nav_menu)): ?>
                    <ul class="col-md-10">
                        <?php foreach ($nav_menu as $key => $value) : ?>
                            <li class="left-nav p-2">
                                <a href="/<?= $value ?>" class="btn-color rounded p-2 text-dark"><?= $key ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <div class="col-md-2">
                        <form action="/" method="post">
                            <input type="hidden" name="session-end" value="1">
                            <input type="submit" style="border: none" class="btn-color rounded p-2 text-dark" value="Se déconnecter" name="deco">
                        </form>
                    </div>
                <?php endif; ?>
            </nav>
        </div>
    </section>
</header>


<main>
    <div class='container'>
