</div>
</main>


<footer>
    <section class=' mt-5 pb-2 h-100'>
    <div class='container'>
        &copy;2022 - L'IDEM <a href="/mentions-legales">Mentions légales</a>
    </div>
    </section>
</footer>
</body>

<script defer>
    if( document.getElementById('reserv-btn') != null ){

    let btnResa = document.getElementById('reserv-btn');
    let formResa = document.getElementById('form-resa');
    let annulBtn = document.getElementById('btn-annul');

    btnResa.addEventListener('click', function () {
        console.log('yo')
        formResa.classList.add('block');
        formResa.classList.remove('none');

        btnResa.classList.add('none')
        btnResa.classList.remove('block')

        annulBtn.classList.add('block');
        annulBtn.classList.remove('none');
    })

    annulBtn.addEventListener('click', function(){
        formResa.classList.add('none');
        formResa.classList.remove('block');


        btnResa.classList.add('block')
        btnResa.classList.remove('none')


        annulBtn.classList.add('none');
        annulBtn.classList.remove('block');

    })
    }

    console.log('Si vous êtes ici, rendez-vous sur la page /easter-egg');
</script>
</html>
