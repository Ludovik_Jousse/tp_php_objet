    <h1 class="annonce-h1">Liste des annonces</h1>

    <ul class="list-container">
    <?php foreach ($list as $key ) :?>

        <li class="card list-card mb-5" style="width: 20rem;">
                    <img class="card-img-top" src="<?php echo '/assets' .DS. 'img' .DS. $key->image ?>" alt="">
                <div class="card-body">
                    <h5 class="card-title"><?php echo $key->name  ?></h5>
                    <p class="card-text"><?php echo $key->description ?></p>
                    <p class="card-text"><?php echo $key->price ?> € </p>
                    <a href="/detail/<?php echo $key->id ?>" class="btn btn-color">
                        <?php echo ($_SESSION['user_type'] == 1) ? 'Voir en détail': ('Je réserve') ;?>
                    </a>
                </div>
        </li>
    <?php endforeach; ?>
    </ul>





