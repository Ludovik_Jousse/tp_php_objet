<h1>Vos annonces favorites</h1>


<?php if(!empty($list)): ?>
<ul class='list-container'>
    <?php foreach ($list as $item) : ?>
    <?php foreach ($item->rentals as $key) : ?>

        <li class="card list-card mb-5" style="width: 20rem;">
            <img class="card-img-top" src="../../assets/img/airbnb.png" alt="">
            <div class="card-body">
                <h5 class="card-title"><?php echo $key->name ?></h5>
                <p class="card-text"><?php echo $key->description ?></p>
                <p class="card-text"><?php echo $key->price ?> € </p>
                <a href="/detail/<?php echo $key->id ?>" class="btn btn-color">
                    <?php echo ($_SESSION['user_type'] == 1) ? 'Voir en détail' : ('Je réserve'); ?>
                </a>
            </div>
        </li>
    <?php endforeach; ?>
    <?php endforeach; ?>
</ul>
<?php else: ?>
<div>
    <p>
        Vous n'avez pas encore d'annonce favorite :c
    </p>
</div>
<?php endif; ?>