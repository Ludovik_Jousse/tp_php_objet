
<?php echo 'La curiosité est un vilain défaut'; sleep(2); ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../assets/css/style2.css">
    <title>js -INTRODUCTION</title>
</head>

<body>
<div>

    <h1>Salut à tous c'est fanta</h1>

</div>
<div class="img">
    <img src="/assets/img/easter_egg/1520321750-1513417948-fanta1.png" alt="">

</div>

<div id="bow" class="rouge none">

    <p>
        NON FANTA POURQUOI TU ES PARTI 😭😭😭😭😭😭😭😭😭😭😭😭😭
    </p>
    <img src="/assets/img/easter_egg/bow.jpeg" alt="">

</div>


<div id="tg" class="tagueule none">
    <p>TA GUEULE !!!</p>
</div>

<div id="nyancat" class="nyancat">
    <img src="/assets/img/easter_egg/cat-nyan-cat.gif" alt="">
</div>

<div id="banane" class="banane">
    <img src="/assets/img/easter_egg/giphy.gif" alt="">
    <p id="text" class="none">It's peanut butter jelly time</p>
</div>

<div id="banane2" class="banane">
    <img src="/assets/img/easter_egg/giphy.gif" alt="">
    <p id="text" class="none">It's peanut butter jelly time</p>
</div>

<div id="kid" class="kid">
    <img src="/assets/img/easter_egg/kid-meme.gif" alt="">
</div>

<div id="slap" class="slap">
    <img src="/assets/img/easter_egg/slaps-everyone.gif" alt="">
</div>

<div id="cat" class="cat">
    <img src="/assets/img/easter_egg/cat-shooting.gif" alt="">
</div>

<div id="sardoche" class="sardoche">
    <img src="/assets/img/easter_egg/sardoche.gif" alt="">
</div>

<div id="pasta" class="pasta">
    <img src="/assets/img/easter_egg/vdr37153.jpg" alt="">
</div>

<div id="bagarre" class="bagarre none">
    <img src="/assets/img/easter_egg/E2yRwe2WQAQ2jYF.jpg" alt="">
    <div id="back" class="back">

    </div>
</div>

</body>

<script>
    function log(e) {
        return console.log(e);
    }


    setInterval(() => {
        let tg = document.getElementById('tg')

        if (tg.className == 'tagueule none') {
            tg.className = 'tagueule'
        } else {
            tg.className = 'tagueule none'
        }


    }, 1000);

    setInterval(() => {
        let bow = document.getElementById('bow')

        if (bow.className == 'rouge none') {
            bow.className = 'rouge'
        } else {
            bow.className = 'rouge none'
        }


    }, 2000);

    let toppercent = -25;

    let leftpercent = 10

    let nyancat = document.getElementById('nyancat');

    setInterval(() => {


        let nyancat = document.getElementById('nyancat');

        nyancat.setAttribute('style', 'left : ' + toppercent + '%; top : ' + leftpercent + '%;')

        toppercent++

        if (toppercent == '110') {

            toppercent = -25
            leftpercent = parseInt(Math.random() * 100);

        }

    }, 50);


    nyancat.addEventListener('click', function (e) {
        document.getElementById('bagarre').className = 'bagarre';
    })

    let back = document.getElementById('back')

    back.addEventListener('click', function (e) {
        document.getElementById('bagarre').className = 'bagarre none';
    })


    setInterval(() => {

        let text = document.getElementById('text')

        let banane = document.getElementById('banane')

        let banane2 = document.getElementById('banane2')

        banane.setAttribute('style', 'top: ' + parseInt(Math.random() * 100) + '%; left: ' + parseInt(Math.random() * 100) + '%;')

        banane2.setAttribute('style', 'left: ' + parseInt(Math.random() * 100) + '%; top: ' + parseInt(Math.random() * 100) + '%;')

        if (text.className == 'none') {
            text.className = ' '
        } else {
            text.className = 'none'
        }
    }, 200);

</script>
</html>