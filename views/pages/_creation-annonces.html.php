<form class="mt-5" method="POST" action="/liste" enctype="multipart/form-data">
    <?php if((!empty($_SESSION['new_annonce_bug']))) : ?>
    <div class='bg-danger'>
        Il y a eu un problème dans votre annonce, veuillez réessayer
    </div>
    <?php endif; ?>
    <div class='form-row'>
        <div class='form-group col-md-4'>
            <label for='inputPays'>Nom de l'annonce</label>
            <input type='text' class='form-control' id='inputName' name="name" placeholder='Nom' required>
        </div>
        <div class='form-group col-md-4'>
            <label for='inputPays'>Pays</label>
            <input type='text' class='form-control' id='inputPays' name="country" placeholder='Pays' required>
        </div>
        <div class='form-group col-md-4'>
            <label for='inputVille'>Ville</label>
            <input type='text' class='form-control' id='inputVille' name="city" placeholder='Ville' required>
        </div>
    </div>

    <div class='form-row'>
        <div class='form-group col-md-3'>
            <label for='inputCity'>Nombre de couchage</label>
            <input type='number' class='form-control' min="1" max="99" id='inputCouchage' placeholder="Couchage ..."
                   name="capacity" required>
        </div>

        <div class='form-group col-md-3'>
            <label for='inputState'>Type de logement</label>
            <select id='inputState' name="type" class='form-control' required>
                <option selected disabled>Choisissez ...</option>
                <option value="1">Logement entier</option>
                <option value='2'>Chambre privée</option>
                <option value='3'>Chambre partagée</option>
            </select>
        </div>

        <div class='form-group col-md-3'>
            <label for='inputCity'>Taille (en m²)</label>
            <input type='text' class='form-control' name="surface" id='inputTaille' placeholder="Taille ..." required>
        </div>

        <div class='form-group col-md-3'>
            <label for='inputPrix'>Prix</label>
            <input type='text' class='form-control' name="price" id='inputPrix' placeholder="Prix ..." required>
        </div>

    </div>

    <div class='form-group'>
        <label for='description'>Description</label>
        <textarea class='form-control' name="description" id='description' rows='3' required
                  placeholder="Petite description de votre annonce ..."></textarea>
    </div>

    <div class='form-group mt-4'>
        <label>Liste des Equipements</label>
        <div class='form-check'>

            <input class='form-check-input' type='checkbox'  name="equipement[]" value="1"  id='grillepain'>
            <label class='form-check-label mr-5' for='grillepain'>
                Grille-pain
            </label>

            <input class='form-check-input' type='checkbox' name='equipement[]' value='2' id='malaver'>
            <label class='form-check-label mr-5' for='malaver'>
                Machine à laver
            </label>

            <input class='form-check-input' type='checkbox' name='equipement[]' value='3' id='four'>
            <label class='form-check-label mr-5' for='four'>
                Four
            </label>

            <input class='form-check-input' type='checkbox' name='equipement[]' value='4'  id='terrasse'>
            <label class='form-check-label mr-5' for='terrasse'>
                Terrasse
            </label>

            <input class='form-check-input' type='checkbox' name='equipement[]' value='5' id='piscine'>
            <label class='form-check-label mr-5' for='piscine'>
                Piscine
            </label>

            <input class='form-check-input' type='checkbox' name='equipement[]' value='5' id='jardin'>
            <label class='form-check-label mr-5' for='jardin'>
                Jardin
            </label>
        </div>
    </div>
    <div class='row form-group'>
        <div class='col-md-4'>
            <label for='reserv-checkin'>Date de début de réservation</label>
        </div>
        <div class='form-group col-md-2'>
            <input type='number' min='<?php echo $date->format('d') ?>' max='31'
                   class='form-control' name='checkin-day' id='reserv-checkin'>
        </div>
        <span>/</span>
        <div class='form-group col-md-2'>
            <input type='number' min='<?php echo $date->format('m') ?>' max='12'
                   class='form-control' name='checkin-month' id='reserv-checkin'>
        </div>
        <span>/</span>
        <div class='form-group col-md-2'>
            <input type='number' min='<?php echo $date->format('Y') ?>' max='2030'
                   class='form-control' name='checkin-year' id='reserv-checkin'>
        </div>
    </div>
    <div class='row form-group'>
        <div class='col-md-4'>
            <label for='reserv-checkout'>Date de fin de réservation</label>
        </div>
        <div class='form-group col-md-2'>
            <input type='number' min='1' max='31' class='form-control' name='checkout-day' id='reserv-checkout'>
        </div>
        <span>/</span>
        <div class='form-group col-md-2'>
            <input type='number' min='1' max='12' class='form-control' name='checkout-month'
                   id='reserv-checkout'>
        </div>
        <span class='text-center'>/</span>
        <div class='form-group col-md-2'>
            <input type='number' min="<?php echo $date->format('Y') ?>" max='2030'
                   class='form-control' name='checkout-year' id='reserv-checkout'>
        </div>

    </div>
    <div class="row">
        <div class="col-md-12">
            <label for="file">Ajouter une photo</label>
            <input type="file" id="file" name="file" accept=".png, .jpg, .jpeg">
        </div>
    </div>

    <button type='submit' class='btn btn-primary mt-3'>Ajouter une annonce</button>
</form>
