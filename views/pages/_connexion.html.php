<div class="mt-5 background-connexion">
    <h1 class="display-3 ml-4">Le formulaire de connexion</h1>
    <?php if (!empty($_SESSION['connexion_bug'])) : ?>
        <div class="bg-danger">
            <p>Mauvais identifiant, veuillez réessayer </p>
        </div>
    <?php endif; ?>
    <div class="form-container d-flex flex-column background-connexion">
        <form class=" p-4 background-connexion" method="POST" action="/liste">
            <div class="row">
                <div class='form-group col-md-6'>
                    <label for='InputEmail1'>Adresse mail</label>
                    <input type='email' class='form-control ' id='InputEmail1' name="mail" aria-describedby='emailHelp'
                           placeholder='Enter email' required>
                    <small id='emailHelp' class='form-text text-muted'>One ne le partagera avec personne</small>
                </div>
                <div class="form-group col-md-6">
                    <label for="InputPassword1">Mot de passe</label>
                    <input type="password" class="form-control" id="InputPassword1" name="pass" placeholder="Password" required>
                </div>

            </div>
            <?php if (isset($_GET['inscription']) == 1): ?>
                <div class="row">
                    <div class='form-group col-md-12'>
                        <label for='InputPassword1'>Nom</label>
                        <input type='text' class='form-control' id='InputName' name='name' placeholder='Nom' required>
                    </div>
                </div>
                <div class='form-check text-center'>
                    <input class='form-check-input' value="2" type='radio' name='user-type' id='Visiteur'>
                    <label class='form-check-label pr-5 mb-3' for='Visiteur' required>Visiteur</label>

                    <input class='form-check-input' value="1" type='radio' name='user-type' id='Annonceur'>
                    <label class='form-check-label mb-3' for='Annonceur' required>Annonceur</label>
                </div>
            <?php endif; ?>
            <div class="form-group form-check text-center">
                <input type="checkbox" class="form-check-input" value="checked" id="Conditions" name="cond">
                <label class="form-check-label" for="Conditions">Vous acceptez les conditions d'utilisation du
                    site</label>
            </div>

            <div class="d-flex justify-content-center">
                <input type="submit" class="btn-lg btn-block btn-color m-0" value="
                    <?php echo (isset($_GET['inscription']) == 1) ? ('Je m\'inscrit') : ('Go !'); ?>
                    ">
            </div>
        </form>
    </div>

    <?php if (!isset($_GET['inscription'])): ?>
        <div class="form-bottom text-center p-3">
            <span class="font-weight-bolder">Pas de compte ?</span> <a href="/?inscription=1">En créer un
                !</a>
        </div>
    <?php endif; ?>
</div>

</div>