<?php foreach ($list as $key) : ?>
<?php if(!empty($_SESSION['booking_bug'])) : ?>
    <div class="bg-danger">
        Il y a eu un problème dans la réservation
    </div>
<?php endif; ?>
<div class="row mt-5">
    <div class="col-md-4">
        <div>
            <img src="<?php echo '/assets' . DS . 'img' . DS . $key->image ?>" class="img-fluid"
                 alt="Photo de l'annonce">
        </div>

        <div>
            <form method="post" action="/favoris">
                <input type="hidden" value="1" name="favorite">
                <input class="btn-color btn " type="submit" value="<?php if (!empty($favorite)) echo '&#10060;' ?> ❤">
            </form>
        </div>
    </div>

    <div class="col md-8">
        <h2 class='font-weight-bold text-uppercase'><?php echo $key->name ?></h2>
            <p>
                <?php echo $key->description ?>
            </p>

            <div class="">
                <h3>Les infos utiles ...</h3>
                <p>
                    <span class='font-weight-bold'>Adresse : </span><?php echo $key->address->city . ' ' .
                        $key->address->country ?> <br/>
                </p>
                <p>
                    <span class='font-weight-bold'>Prix : </span><?php echo $key->price ?> €<br/>
                </p>
                <p>
                    <span class='font-weight-bold'>Taille du bien : </span><?php echo $key->surface ?> m²<br/>
                </p>
                <p>
                    <span class='font-weight-bold'>Nombre de couchage : </span><?php echo $key->capacity ?><br/>
                </p>
                <p>
                    <span class='font-weight-bold'>Equipements : </span>
                    <ul>

                    <?php foreach ($key->equipment as $item): ?>
                        <li>
                            <?php  echo $item->label ?>
                        </li>
                    <br/>
                    <?php endforeach; ?>
                </ul>
                </p>
                <p>
                    <span class='font-weight-bold' >Date de disponibilitées : </span>
                    Du <?php  echo $key->start_date ?> au <?php echo $key->end_date ?> <br/>
                </p>
            </div>

        <?php endforeach; ?>
        <div>
            <?php if($_SESSION['user_type']== 2) : ?>
            <button class="btn btn-color" id="reserv-btn">Je réserve ce bien</button>
            <?php endif; ?>
        </div>

        <form id="form-resa" class="none" method="post" action="/liste">

            <input type="hidden" name="resa" value="1">
            <div class="row">
                <div class="col-md-4">
                    <label for="reserv-checkin">Date de début de réservation</label>
                </div>
                <div class="form-group col-md-2">
                    <input type="number" min='<?php echo $date->format('d') ?>' max='31'
                           class="form-control" name="checkin-day" id="reserv-checkin">
                </div>
                <span>/</span>
                <div class="form-group col-md-2">
                    <input type="number" min='<?php echo $date->format('m') ?>' max='12'
                           class="form-control" name="checkin-month" id="reserv-checkin">
                </div>
                <span>/</span>
                <div class="form-group col-md-2">
                    <input type="number" min='<?php echo $date->format('Y') ?>' max='2030'
                           class="form-control" name="checkin-year" id="reserv-checkin">
                </div>
            </div>
            <div class='row'>
                <div class='col-md-4'>
                    <label for='reserv-checkout'>Date de fin de réservation</label>
                </div>
                <div class="form-group col-md-2">
                    <input type='number' min='1' max="31" class="form-control" name='checkout-day' id='reserv-checkout'>
                </div>
                <span>/</span>
                <div class="form-group col-md-2">
                    <input type='number' min='1' max="12" class="form-control" name='checkout-month'
                           id='reserv-checkout'>
                </div>
                <span class="text-center">/</span>
                <div class="form-group col-md-2">
                    <input type='number' min="<?php echo $date->format('Y') ?>" max="2030"
                           class="form-control" name='checkout-year' id='reserv-checkout'>
                </div>

            </div>
            <input class="btn btn-color" type="submit" value="Je réserve">

        </form>

        <button class='btn btn-color none' id="btn-annul" value='Je réserve'>
            Annuler
        </button>

    </div>
</div>
