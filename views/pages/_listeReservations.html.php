<h1 class='annonce-h1'>Liste de vos réservations</h1>


<?php if (!empty($list)) : ?>
    <table style="width: 100%">
        <thead>
        <tr>
            <th class="p-2">Nom chambre</th>
            <th class='p-2'>Réserveur</th>
            <th class='p-2'>Date de début</th>
            <th class='p-2'>Date de fin</th>
        </tr>
        </thead>
        <tbody>
        <?php
            foreach ($list as $key):
            foreach ($key->rentals as $rental):
            ?>

        <tr class="m-3">
            <td class="p-2"><?php echo $rental->name ?></td>
            <td class="p-2"> <?php echo $key->users->name  ?></td>
            <td class="p-2"><?php echo $key->check_in  ?></td>
            <td class="p-2"><?php echo $key->check_out  ?></td>
            <td class="p-2 m-2  btn btn-color">
                <a class="text-dark" href="/detail/<?php echo $key->rental_id ?>">Voir ce bien</a>
            </td>
        </tr>

        <?php endforeach; ?>
        <?php endforeach; ?>

        </tbody>
    </table>
<?php  else : ?>
<div>
    <p>Il n'y a rien ici :c</p>
</div>

<?php endif; ?>